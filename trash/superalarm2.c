#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <string.h>


#include "confparser.h"
#include "superalarm.h"
#include "sdl_addon.h"

Alarm* getAlarmID(int aid, Context* cx)
{
	int i;
	Alarm* ptr = cx->alarms;
	
	for (i = 0; i < cx->alarmCount; i++)
	{
		if (ptr->id == aid)
			return ptr;
		ptr = ptr->next;
	}
}

int alarmThread(void* userPtr)
{
	Context* c = (Context*)userPtr;
	time_t nextWake = 0;
	int i;
	Alarm* alrmPtr = c->alarms;
	Alarm* curAlarm = 0;
	
	if (c->alarmCount == 0)
		return -1;
	
	printf("AlarmThread Started.\n");
	
	while (1)
	{
		curAlarm = 0;
		nextWake = 0;
		alrmPtr = c->alarms;
		printf("Alarm Count: %i\n",c->alarmCount);
		for (i = 0; i < c->alarmCount; i++)
		{
			if ((nextWake > alrmPtr->epoch || nextWake == 0) && alrmPtr->state == 0)
			{
				nextWake = alrmPtr->epoch;
				printf("LOOK: %i\n",alrmPtr->epoch);
				curAlarm = alrmPtr;
			}
			alrmPtr = alrmPtr->next;
		}
		
		if (nextWake == 0)
			return -1;
		
		if ((nextWake-time(0)) < 0)
		{
			pthread_cancel(&c->threadServer);
			changeProperty(c->ini,curAlarm->sectionIndex,"STATE","MISSED");
			buildINI(c->ini,c->iniPath);
			rebuildState(c);
			pthread_create(&c->threadServer,NULL,&serverThread,(void*)c);
			continue;
		}
		
		printf("%s will wake in %i seconds\n",curAlarm->name,nextWake-time(0));
		sleep(nextWake-time(0));
		if (nextWake == time(0))
		{
			pthread_cancel(&c->threadServer);
			printf("Alarm Expired, %s\n",curAlarm->name);
			sdl_play_sound("test.wav");
			removeSection(c->ini,curAlarm->sectionIndex);
			buildINI(c->ini,c->iniPath);
			rebuildState(c);
			pthread_create(&c->threadServer,NULL,&serverThread,(void*)c);
			return;
		}
		else
		{
			printf("Interrupted!\n");
			continue;
		}
	}
}

int serverThread(void* userPtr)
{
	Context* cx = (Context*) userPtr;
	
	printf("ServerThread Started, Port: %i\n",cx->port);
	if (cx->listener == 0)
	{
		cx->listener = socket(AF_INET,SOCK_STREAM, 0);
		struct in_addr ipAddr;
			ipAddr.s_addr = INADDR_ANY;
		struct sockaddr_in serverAddress;
			serverAddress.sin_family = AF_INET;
			serverAddress.sin_port = htons(cx->port);
			serverAddress.sin_addr = ipAddr;
		bind(cx->listener,(struct sockaddr*)&serverAddress,(socklen_t)sizeof(struct sockaddr_in));
		listen(cx->listener,32);
	}
	                                                                                                                                                                                                                                                                           
	while (1)
	{
		struct sockaddr_in clientInfo;
		socklen_t clientInfoSize = sizeof(struct sockaddr_in);
		
		int clientSocket = accept(cx->listener,(struct sockaddr*)&clientInfo,&clientInfoSize);
		
		pthread_cancel(&cx->threadAlarm);  
		
		printf("Connection.\n");
		
		Packet* newPack = malloc(sizeof(Packet));
		
		read(clientSocket,newPack,sizeof(Packet));
		
		if (newPack->magic == MAGIC_NUMBER)
		{
			printf("ALRM M5essage Received.\n");
			
			switch (newPack->cmd)
			{
				case CMD_UPDATE:
					//FCMD_UPDATE(newPack,cx,clientSocket);
					break;
				case CMD_RETRIEVE:
					FCMD_RETRIEVE(newPack,cx,clientSocket);
					break;
				case CMD_RETRIEVE1:
					FCMD_RETRIEVE1(newPack,cx,clientSocket);
					break;
				case CMD_DELETEID:
					FCMD_DELETEID(newPack,cx,clientSocket);
					break;
				case CMD_LISTSOUND:
					FCMD_LISTSOUND(newPack,cx,clientSocket);
					break;
				case CMD_ADDALARM:
					FCMD_ADDALARM(newPack,cx,clientSocket);
					break;
				default:
					printf("Unknown/bad Command, %i.\n",newPack->cmd);
					break;
			}
			
			pthread_create(&cx->threadAlarm,NULL,&alarmThread,(void*)cx);
		}
		
		close(clientSocket);
	}
}


int rebuildState(Context* cx)
{
	int i,x, id = 1000;
	ConfFile* ini = parseINI(cx->iniPath);
	Alarm* alarmPtr = cx->alarms;
	
	//buildINI(ini,"test.ini");
	
	if (ini == NULL)
		return -1;
	
	if (alarmPtr != NULL)
	{
		Alarm* tPtr;
		
		for (i = 0; i < cx->alarmCount; i++)
		{
			printf("%i, %i\n",i,cx->alarmCount);
			//flush();
			tPtr = alarmPtr->next;
			free(alarmPtr);
			alarmPtr = tPtr;
		}
	}
	
	cx->alarmCount = 0;
	cx->alarms = malloc(sizeof(Alarm));
	alarmPtr = cx->alarms;
	
	for (i = 0; i < ini->sectionCount; i++)
	{
		if (strcmp(ini->sections[i].name,"ALARM") == 0)
		{
			for (x = 0; x < ini->sections[i].propertyCount; x++)
			{
				if (strcmp(ini->sections[i].properties[x].name,"NAME") == 0)
				{
					alarmPtr->name = malloc(strlen(ini->sections[i].properties[x].value)+1);
					memcpy(alarmPtr->name,ini->sections[i].properties[x].value,strlen(ini->sections[i].properties[x].value)+1);
				}
				else if (strcmp(ini->sections[i].properties[x].name,"EPOCH") == 0)
					alarmPtr->epoch = atoi(ini->sections[i].properties[x].value);
				else if (strcmp(ini->sections[i].properties[x].name,"TYPE") == 0)
					alarmPtr->type = atoi(ini->sections[i].properties[x].value);
				else if (strcmp(ini->sections[i].properties[x].name,"ACTION") == 0)
					alarmPtr->action = atoi(ini->sections[i].properties[x].value);
				else if (strcmp(ini->sections[i].properties[x].name,"STATE") == 0)
				{
					if (strcmp(ini->sections[i].properties[x].value,"ACTIVE") == 0)
						alarmPtr->state = 0;
					if (strcmp(ini->sections[i].properties[x].value,"MISSED") == 0)
						alarmPtr->state = 1;
					if (strcmp(ini->sections[i].properties[x].value,"DISABLED") == 0)
						alarmPtr->state = 2;
				}
				else
					printf("Rubbish property %s\n",ini->sections[i].properties[x].name);
			}
			
			alarmPtr->id = id++;
			alarmPtr->sectionIndex = i;
			alarmPtr->next = malloc(sizeof(Alarm));
			alarmPtr = alarmPtr->next;
			cx->alarmCount++;
		}
		else if (strcmp(ini->sections[i].name,"CONF") == 0)
		{
			for (x = 0; x < ini->sections[i].propertyCount; x++)
			{
				if (strcmp(ini->sections[i].properties[x].name,"PORT") == 0)
					cx->port = atoi(ini->sections[i].properties[x].value);
				else
					printf("Rubbish property %s\n",ini->sections[i].properties[x].name);
			}
		}
		else if (strcmp(ini->sections[i].name,"SOUND") == 0)
		{
			for (x = 0; x < ini->sections[i].propertyCount; x++)
			{
				if (strcmp(ini->sections[i].properties[x].name,"PATH") == 0)
				{
					cx->soundPath = malloc(strlen(ini->sections[i].properties[x].value)+1);
					strcpy(cx->soundPath,ini->sections[i].properties[x].value);
				}
				else
					printf("Rubbish property %s\n",ini->sections[i].properties[x].name);
			}
		}
		else
			printf("Rubbish Section %s\n",ini->sections[i].name);
	}
	
	alarmPtr = cx->alarms;
	
	printf("Config loaded, %i alarms found:",cx->alarmCount);
	for (i = 0; i < cx->alarmCount; i++)
	{
		printf(" %s,",alarmPtr->name);
		alarmPtr = alarmPtr->next;
	}
	printf("\n");
	
	free(cx->ini);
	cx->ini = ini;
	
	return 0;
}

int main()
{		
	Context* cx = malloc(sizeof(Context));
	cx->alarms = malloc(sizeof(Alarm));
	cx->alarmCount;
	cx->soundPath="./";
	cx->iniPath = "config.ini";
	cx->ini = NULL;
	cx->listener = 0;
	
	sdl_init();
	
	if (rebuildState(cx) == -1)
	{
		printf("Failed to build state, bad INI filename\n");
		return -1;
	}	
	
	pthread_create(&cx->threadServer,NULL,&serverThread,(void*)cx);
	pthread_create(&cx->threadAlarm,NULL,&alarmThread,(void*)cx);
	
	pthread_exit(NULL);
	
	printf("Super Alarm Service has ended.\n");
	
	return 0;
}
