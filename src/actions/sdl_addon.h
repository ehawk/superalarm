#ifndef _SDL_ADDON_
#define _SDL_ADDON_

#ifdef USE_SDL

#include <SDL2/SDL.h>

typedef struct
{
	Uint8*		stream;
	Uint32		streamOffset;
	Uint32		streamSize;
} AudioStream;

typedef struct
{
	SDL_AudioDeviceID	dev;
	Uint8				inUse;
} AudioContext;

extern AudioContext* cxt;

void wavInit();
void wavDeinit();

void playWAV(char* snd);

#endif

#endif
