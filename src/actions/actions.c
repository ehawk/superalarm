#include "../superalarm.h"
#include "actions.h"

#include <stdlib.h>
#include <stdio.h>

Action* actionSound(char* path)
{
	Action* action = malloc(sizeof(Action));
	
	action->name = "Play Sound";
	action->onExpire = actionSoundExpire;
	action->onCancel = actionSoundDelete;
	
	return action;
}


void actionSoundExpire(Alarm* a, void* data)
{
	playWAV(data);
	printf("PLAYING: %s\n",data);
}

void actionSoundDelete(Alarm* a, void* data)
{
	
}
