#ifndef USE_SDL

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <alsa/asoundlib.h>

#include "wave.h"

snd_pcm_t* handle = 0;
WAVE* wave = 0;
char* empty;


void wavInit()
{
	const char* dev = "default";
	snd_pcm_sframes_t frames;
	empty = malloc(PCM_PERIOD);
	memset(empty,0,PCM_PERIOD);
	
	snd_async_handler_t* hnd;
	
	if (snd_pcm_open(&handle,dev,SND_PCM_STREAM_PLAYBACK, 0) < 0)
		return;
}

void wavDeinit()
{
	snd_pcm_close(handle);
}

int wavSetupDesc(struct pollfd** pfd, int extraSlots)
{
	int count = snd_pcm_poll_descriptors_count(handle);
	*pfd = malloc(sizeof(struct pollfd) * (count+extraSlots));
	return snd_pcm_poll_descriptors(handle,*pfd,count) + extraSlots;
}

int wavFill()
{	
	if ((wave->samplesOffset+PCM_PERIOD* wave->fmt.wavBitDepth/8 * wave->fmt.wavChannels) < wave->samplesCount) 
	{
		wave->samplesOffset += snd_pcm_writei(handle, wave->samples+wave->samplesOffset, PCM_PERIOD) * wave->fmt.wavBitDepth/8 * wave->fmt.wavChannels;
		return 0;
	}
	else
	{
		snd_pcm_drain(handle);
		free(wave->samples);
		free(wave);
		wave = 0;
		return 1;
	}
}

WAVE* loadWAV(char* wavPath)
{
	FILE* wavFile = fopen(wavPath,"r");
	
	if (wavFile == NULL)
		return NULL;
	
	RIFFHeader header;
	printf("SIZE: %i\n",sizeof(RIFFHeader));
	
	fread(&header,sizeof(RIFFHeader),1,wavFile);
	
	if (header.chunkMagic == 0x46464952)
	{
		printf("Valid RIFF Container, of Size: %ikb ",header.chunkLength/1024);
		
		if (header.chunkTag == 0x45564157)
		{
			printf("and is a WAVE Container\n");
		}
		else
		{
			printf("and is an UNKNOWN Container\n");
			return 0;
		}
	}
	
	wave = malloc(sizeof(WAVE));
	WAVSamples smpls;
	
	printf("SIZE: %i\n",sizeof(WAVFormat));
	fread(wave,sizeof(WAVFormat),1,wavFile);
	fread(&smpls,sizeof(WAVSamples),1,wavFile);
	
	wave->samples = malloc(smpls.wavLength);
	fread(wave->samples,smpls.wavLength,1,wavFile);
	
	wave->samplesCount = smpls.wavLength;
	wave->samplesOffset = 0;
	
	if (wave->fmt.wavBitDepth == 16)
		wave->fmt.wavFormat = SND_PCM_FORMAT_S16;
	else if (wave->fmt.wavBitDepth == 8)
		wave->fmt.wavFormat = SND_PCM_FORMAT_U8;
	
	fclose(wavFile);
	
	return wave;
}

void playWAV(char* filePath)
{	
	if (wave != NULL)
	{
		free(wave->samples);
		free(wave);
	}
	
	WAVE* wave = loadWAV(filePath);
	
	if (wave == NULL)
		return;
	
	snd_pcm_set_params(handle,wave->fmt.wavFormat,SND_PCM_ACCESS_RW_INTERLEAVED,wave->fmt.wavChannels,wave->fmt.wavFrequency,1,500000);
}

#endif
