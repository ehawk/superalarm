#ifndef _ACTIONS_H_
#define _ACTIONS_H_

//Action factories

Action* actionSound(char* path);


Action* actionNotify();


Action* actionEmail();


Action* actionExternal();

//Sound actions
void actionSoundExpire(Alarm* a, void* data);
void actionSoundDelete(Alarm* a, void* data);

#endif
