#ifndef _WAVE_H_
#define _WAVE_H_

#ifndef USE_SDL

#include <alsa/asoundlib.h>

#define PCM_PERIOD 4096

typedef struct
{
	uint32_t	chunkMagic;
	uint32_t	chunkLength;
	uint32_t	chunkTag;
} RIFFHeader;

typedef struct
{
	uint32_t	wavMagic;
	uint32_t	wavLength;
	uint16_t	wavFormat;
	uint16_t	wavChannels;
	uint32_t	wavFrequency;
	uint32_t	wavByteRate;
	uint16_t	wavBlockAlign;
	uint16_t	wavBitDepth;
} WAVFormat;

typedef struct
{
	uint32_t	wavMagic;
	uint32_t	wavLength;
} WAVSamples;

typedef struct
{
	WAVFormat	fmt;
	uint32_t	samplesCount;
	uint32_t	samplesOffset;
	char*		samples;
} WAVE;

WAVE* loadWAV(char* wavPath);
void pcmFill(snd_async_handler_t* hnd);

void playWAV(char* filePath);
void wavDeinit();
void wavInit();
int wavFill();
int wavSetupDesc(struct pollfd** pfd, int extraSlots);

extern snd_pcm_t* handle;
extern WAVE* wave;
extern char* empty;

#endif
#endif
