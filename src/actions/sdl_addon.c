#ifdef USE_SDL

#include "sdl_addon.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>

AudioContext* cxt;

void wavInit()
{
	SDL_Init(SDL_INIT_AUDIO);
	
	cxt = malloc(sizeof(AudioContext));
	cxt->inUse = 0;
	
	int i;
	printf("Listing Audio Devices:\n");
	for (i = 0; i < SDL_GetNumAudioDevices(0); i++)
	{
		printf("Device[%i]: %s\n",i,SDL_GetAudioDeviceName(i,0));
	}
}

void wavDeinit()
{
	if (cxt->inUse)
		SDL_CloseAudioDevice(cxt->dev);
	free(cxt);
	SDL_Quit();
}

void bufferAudio(void *userdata, Uint8 *buffer, int len)
{
	AudioStream* stream = (AudioStream*) userdata;
	
	if (stream->streamOffset < stream->streamSize && stream->streamOffset+len < stream->streamSize)
	{
		memcpy(buffer,(stream->stream+stream->streamOffset),len);
		stream->streamOffset += len;
	}
	else
	{
		memset(buffer,0,len);
	}
}

void playWAV(char* snd)
{
	if(cxt->inUse)
		SDL_CloseAudioDevice(cxt->dev);
	
	AudioStream* audio = malloc(sizeof(AudioStream));
	audio->streamOffset = 0;
	
	SDL_AudioDeviceID dev;
	SDL_AudioSpec wavSpec;
	SDL_AudioSpec* spec;
	//SDL_zero(wavSpec);
	
	spec = SDL_LoadWAV(snd,&wavSpec,&audio->stream,&audio->streamSize);
	if (spec == 0)
	{
		printf("Failed to open wav: %s\n",snd);
		return;
	}
	
	spec->callback = bufferAudio;
	spec->userdata = audio;
	printf("Freq: %i, Format: %i, Channels: %i, Samples: %i\n",spec->freq,spec->format,spec->channels,spec->samples);
		
	cxt->dev = SDL_OpenAudioDevice(NULL, 0, spec, &wavSpec, 0);
	
	if (cxt->dev == 0)
	{
		printf("Failed to open sound device: %s\n",SDL_GetError());
		return;
	}
	
	SDL_PauseAudioDevice(cxt->dev,0);
	
	//SDL_Delay(500);
	
	cxt->inUse = 1;
}

#endif
