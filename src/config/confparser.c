#include "confparser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

ConfFile* parseINI(char* file)
{
	FILE* cc = fopen(file,"r");
	if (cc == NULL)
		return NULL;
	int tok = fgetc(cc);
	ConfFile* cf = malloc(sizeof(ConfFile));
	SectionPropertyCount* cs = 0;
	SectionPropertyCount* fcs = 0;
	
	cf->sectionCount = 0;
	
	while (tok != EOF)
	{
		switch (tok)
		{
			case '[':
				cf->sectionCount++;
				if (cs == 0)
				{
					cs = malloc(sizeof(SectionPropertyCount));
					cs->propertyCount=0;
					fcs = cs;
				}
				else
				{
					cs->next = malloc(sizeof(SectionPropertyCount));
					cs = cs->next;
					cs->propertyCount=0;
				}
				break;
			case '=':
				cs->propertyCount++;
				break;
		}
		
		tok = fgetc(cc);
	}
	
	rewind(cc);
	tok = fgetc(cc);
	cf->sections = malloc(sizeof(Section)*cf->sectionCount);
	cs = fcs;
	
	uint8_t mode = 0;
	uint32_t i = -1;
	uint32_t p = -1;
	uint8_t xx = 0;
	
	while (tok != EOF)
	{
		switch (tok)
		{
			case '[':
				if (mode == 0)
				{
					i++;
					mode = MODE_SECTION;
					cf->sections[i].name = malloc(255);
					cf->sections[i].properties = malloc(sizeof(Property)*fcs->propertyCount);
					cf->sections[i].propertyCount = fcs->propertyCount;
					cs = fcs->next;
					free(fcs);
					fcs = cs;
					xx = 0;
					p = 0;
				}
				break;
			case ']':
				if (mode == MODE_SECTION)
				{
					mode = 0;
					cf->sections[i].name[xx] = 0;
				}
				break;
			case '=':
				if (mode == MODE_PROPERTY_NAME)
				{
					cf->sections[i].properties[p].name[xx] = 0;
					mode = MODE_PROPERTY_VALUE;
					xx = 0;
				}
				break;
			case '\n':
				if (mode == MODE_PROPERTY_VALUE)
				{
					cf->sections[i].properties[p].value[xx] = 0;
					printf("END: %c, %s\n",cf->sections[i].properties[p].value[xx-1],cf->sections[i].properties[p].value);
					mode = 0;
					p++;
				}
				break;
			case ' ':
			case '\t':
				if (mode != MODE_PROPERTY_VALUE)
					break;
			default:
				if (mode == MODE_SECTION && xx < 255)
				{
					cf->sections[i].name[xx++] = tok;
				}
				else if (mode == MODE_PROPERTY_NAME && xx < 255)
				{
					cf->sections[i].properties[p].name[xx++] = tok;
				} 
				else if (mode == MODE_PROPERTY_VALUE && xx < 255)
				{
					cf->sections[i].properties[p].value[xx++] = tok;
				}
				else if (mode == 0)
				{
					cf->sections[i].properties[p].name = malloc(255);
					cf->sections[i].properties[p].value = malloc(255);
					mode = MODE_PROPERTY_NAME;
					xx = 0;
					cf->sections[i].properties[p].name[xx++] = tok;
				}
				break;
		}
		tok = fgetc(cc);
	}
	
	fclose(cc);
	return cf;
}

void buildSection(ConfFile* ini, char* sectionName, int elements, ...)
{
	int i;
	va_list props;
	
	ini->sections = realloc(ini->sections,(ini->sectionCount+1)*sizeof(Section));
	ini->sectionCount++;
	ini->sections[ini->sectionCount-1].name = malloc(strlen(sectionName)+1);
	strcpy(ini->sections[ini->sectionCount-1].name,sectionName);
	           
	va_start(props,elements);
	ini->sections[ini->sectionCount-1].properties = malloc(elements*sizeof(Property));
	ini->sections[ini->sectionCount-1].propertyCount = elements;
	for (i = 0; i < elements; i++)
	{
		char* n = (char*) va_arg(props,char*);
		char* v = (char*) va_arg(props,char*);
		ini->sections[ini->sectionCount-1].properties[i].name = malloc(strlen(n)+1);
		strcpy(ini->sections[ini->sectionCount-1].properties[i].name,n);
		ini->sections[ini->sectionCount-1].properties[i].value = malloc(strlen(v)+1);
		strcpy(ini->sections[ini->sectionCount-1].properties[i].value,v);
	}
	
	va_end(props);
}

void removeSection(ConfFile* ini, int sectionIndex)
{
	//Section* temp = malloc(sizeof(Section));
	//memcpy(temp,ini->sections[sectionIndex],sizeof(Section));
	//memcpy(&ini->sections[sectionIndex],&ini->sections[ini->sectionCount-1],sizeof(Section));
	//ini->sectionCount--;
	//ini->sections = realloc(ini->sections,ini->sectionCount*sizeof(Section));
	printf("Doing Something!\n");
	free(ini->sections[sectionIndex].name);
	//ini->sections[sectionIndex].name = 0;
	free(ini->sections[sectionIndex].properties);
	//ini->sections[sectionIndex].properties = 0;
	//ini->sections[sectionIndex].propertyCount = 0;
	memset((ini->sections+sectionIndex),0,12);
}

void changeProperty(ConfFile* ini, int index, char* prop, char* value)
{
	int i;
	for (i = 0; i < ini->sections[index].propertyCount; i++)
	{
		if (strcmp(ini->sections[index].properties[i].name,prop) == 0)
		{
			free(ini->sections[index].properties[i].value);
			ini->sections[index].properties[i].value = malloc(strlen(value)+1);
			strcpy(ini->sections[index].properties[i].value,value);
			return;
		}
	}
}

void buildINI(ConfFile* ini, char* output)
{
	int i, x;
	FILE* file = fopen(output,"w");
	
	for (i = 0; i < ini->sectionCount; i++)
	{
		if (ini->sections[i].name == 0)
			continue;
		
		fprintf(file,"[%s]\n",ini->sections[i].name);
		printf("[%s]\n",ini->sections[i].name);
		for (x = 0; x < ini->sections[i].propertyCount; x++)
		{
			fprintf(file,"%s=%s\n",ini->sections[i].properties[x].name,ini->sections[i].properties[x].value);
			printf("%s=%s\n",ini->sections[i].properties[x].name,ini->sections[i].properties[x].value);
		}
		fprintf(file,"\n");
		printf("\n");
	}
	fclose(file);
}
