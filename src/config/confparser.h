#ifndef _CONFPARSER_H_
#define _CONFPARSER_H_

#include <stdint.h>

#define MODE_INVALID			001
#define MODE_SECTION 			100
#define MODE_PROPERTY_NAME		200
#define MODE_PROPERTY_VALUE		201

typedef struct
{
	char*		name;
	char*		value;
} Property;

typedef struct
{
	char* 		name;
	uint32_t	propertyCount;
	Property*	properties;
} Section;

typedef struct
{
	uint32_t 	sectionCount;
	Section*	sections;
} ConfFile; 

typedef struct SectionPropertyCount
{
	uint32_t propertyCount;
	struct SectionPropertyCount* next;
} SectionPropertyCount;

ConfFile* parseINI(char* file);
void buildSection(ConfFile* ini, char* sectionName, int elements, ...);
void changeProperty(ConfFile* ini, int index, char* property, char* value);
void buildINI(ConfFile* ini, char* output);

#endif
