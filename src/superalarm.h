#ifndef _SUPERALARM_H_
#define _SUPERALARM_H_

#include "config/confparser.h"

#include <stdint.h>
#include <time.h>

#define MAGIC_NUMBER 0x4D524C41

#define CMD_UPDATE			100
#define CMD_RETRIEVE		200
#define CMD_RETRIEVE1		202
#define CMD_DELETEID		204
#define CMD_LISTSOUND		300
#define CMD_ADDALARM		400
#define	CMD_ADDACTION		500

#define TYPE_MONOTONIC		100
#define	TYPE_CONTINUOUS		101

#define ACTION_SOUND		100
#define	ACTION_EMAIL		101
#define ACTION_EXTERNAL		102
#define	ACTION_NOTIFICATION	103

#define STATE_ACTIVE		0
#define STATE_MISSED		1
#define STATE_DISABLED		2

typedef struct Alarm
{
	char* 		name;
	uint32_t	epoch;
	uint8_t		type;
	uint16_t	id;
	uint16_t	sectionIndex;
	uint8_t		state;
	struct Alarm* next;
} Alarm;

typedef struct Sound
{
	uint8_t 	nameSize;
	char* 		name;
	struct Sound*	next;
} Sound;

typedef struct
{
	uint8_t 	nameSize;
	char 		name[];
} StringField;

typedef struct
{
	char* 		name;
	void*		data;
	uint32_t	alarmID;
	uint32_t	sectionIndex;
	void		(*onExpire)(Alarm*,void*);
	void		(*onCreate)(Alarm*,void*);
	void		(*onCancel)(Alarm*,void*);
} Action;

typedef struct
{
	uint16_t	port;
	char*		iniPath;
	char*		soundPath;
	uint32_t	alarmCount;
	Alarm* 		alarms;
	ConfFile*	ini;
	int			listener;
	int			nextFreeID;
	Alarm*		nextTrigger;
	Action*		actionData;
	uint32_t	actionCount;
} Context;

typedef struct
{
	uint32_t 	magic;
	uint32_t 	cmd;
	uint32_t 	flags;
	uint32_t	size;
} Packet;

typedef struct
{
	uint32_t	epoch;
	uint16_t	id;
	uint8_t		type;
	uint8_t		state;
	uint8_t		nameSize;
	char 		name[];
} AlarmData;

typedef struct
{
	uint32_t	magic;
	uint32_t	cmd;
	uint32_t	flags;
	uint32_t	size;
	AlarmData	data;
} PacketReponseAlarm;

typedef struct
{
	uint32_t 	magic;
	uint32_t 	cmd;
	uint32_t 	flags;
	uint32_t 	dataSize;
	uint32_t	dataCount;
	AlarmData	data[];
} PacketAlarmUpdate;

typedef struct
{
	uint32_t 	magic;
	uint32_t 	cmd;
	uint32_t 	flags;
	uint32_t 	dataSize;
	uint32_t	dataCount;
	StringField	data[];
} PacketListSound;

typedef struct
{
	uint32_t	epoch;
	uint8_t		type;
	uint8_t		action;
} PacketAddAlarm;

// Functions that carry out the defined commands, implemented in functions.c
void FCMD_DELETEID(Packet* p, Context* cx, int clientSock);
void FCMD_ADDALARM(Packet* p, Context* cx, int clientSock);
void FCMD_ADDACTION(Packet* p, Context* cx, int clientSock);
void FCMD_LISTSOUND(Packet* p, Context* cx, int clientSock);
void FCMD_RETRIEVE(Packet* p, Context* cx, int clientSock);
void FCMD_RETRIEVE1(Packet* p, Context* cx, int clientSock);

// Main functions that handle the socket interface and alarm scheduling
Alarm* getAlarmID(int aid, Context* cx);
char* char_replace(char pattern, char replacment, char* string);
char* fixspace_string(char* string);

void onWait(Context* cx);		// Main loop, waits for either a packet to arrive or an alarm to elapse
void onConnect(Context* cx);	// Called when a packet arrives
void onAlarm(Context* cx);		// Called to test and trigger alarm (however the alarm is not guaranteed to be ready)

#endif
