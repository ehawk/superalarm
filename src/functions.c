#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <dirent.h>
#include "superalarm.h"

//Delete an alarm by ID
void FCMD_DELETEID(Packet* p, Context* cx, int clientSock)
{
	int i;
	Alarm* ptr = cx->alarms;
	
	for (i = 0; i < cx->alarmCount; i++)
	{
		if (ptr->id == p->size)
		{
			removeSection(cx->ini,ptr->sectionIndex);
			buildINI(cx->ini,cx->iniPath);
			rebuildState(cx);
			return;
		}
		ptr = ptr->next;
	}
}

//Add a new alarm
void FCMD_ADDALARM(Packet* p, Context* cx, int clientSock)
{
	char* data = malloc(p->size);
	read(clientSock,data,p->size);
	PacketAddAlarm* paa = (PacketAddAlarm*)data;
	StringField* name = (StringField*)(data+6);
	//int* repeat = (int*)(char*)(data+p->size-4);
	char* epochStr = malloc(33);
	//char* repeatStr = malloc(33);
	char* idStr = malloc(6);
	char* nameClean = malloc(name->nameSize+1);
	char* flagsStr = malloc(2);
	
	memcpy(nameClean,name->name,name->nameSize);
	nameClean[name->nameSize] = 0;
	
	printf("AddAlarm T: %i A: %i \n",paa->type,paa->action);
	
	snprintf(epochStr,33,"%i",paa->epoch);
	snprintf(flagsStr,2,"%i",((p->flags & ~0x1) & 0x7)>>1);
	snprintf(idStr,6,"%i",cx->nextFreeID++);
	
	if (paa->type == TYPE_MONOTONIC)
	{
		buildSection(cx->ini,"ALARM",5,"NAME",nameClean,"EPOCH",epochStr,"TYPE","100","ID",idStr,"STATE",flagsStr);
		buildINI(cx->ini,cx->iniPath);
		rebuildState(cx);
	}
	
	free(data);
	free(epochStr);
	//free(repeatStr);
	free(nameClean);
	free(flagsStr);
	free(idStr);
	
	if (p->flags&0x1 == 1)
	{
		Packet* response = malloc(sizeof(Packet));
		response->magic = MAGIC_NUMBER;
		response->cmd = CMD_ADDALARM+1;
		response->flags = cx->nextFreeID-1;
		response->size = 0;
		
		write(clientSock,response,sizeof(Packet));
		free(response);
	}
}

//Returns a list of all sounds listed in the sound path (see config.ini)
void FCMD_LISTSOUND(Packet* p, Context* cx, int clientSock)
{
	DIR* soundPath = opendir(cx->soundPath);
	Sound* base;
	Sound* cur;
	base = malloc(sizeof(Sound));
	cur = base;
	int i;
	int size = 0;
	int count = 0;
	while (1)
	{
		struct dirent* pth = readdir(soundPath);
		if (pth == 0)
			break;
		if (strcmp(pth->d_name,".") == 0 || strcmp(pth->d_name,"..") == 0)
			continue;
		count++;
		cur->nameSize = strlen(pth->d_name);
		cur->name = malloc(cur->nameSize);
		strcpy(cur->name,pth->d_name);
		size += cur->nameSize;
		cur->next = malloc(sizeof(Sound));
		cur = cur->next;
	}
	
	cur = base;
	PacketListSound* pls = malloc(sizeof(PacketListSound) + count*sizeof(StringField) + size);
	pls->magic = MAGIC_NUMBER;
	pls->cmd = CMD_LISTSOUND+1;
	pls->flags = 0;
	pls->dataCount = count;
	pls->dataSize = count*sizeof(StringField) + size;
	char* off = (char*)(pls->data);
	for (i = 0; i < count; i++)
	{
		StringField* ptr = (StringField*)off;
		ptr->nameSize = cur->nameSize;
		memcpy(ptr->name,cur->name,cur->nameSize);
		off += (sizeof(StringField)+cur->nameSize);
		cur = cur->next;
	}
	
	write(clientSock,pls,sizeof(PacketListSound) + count*sizeof(StringField) + size);
}

//retrieves the details of one alarm bases on a given ID
void FCMD_RETRIEVE1(Packet* p, Context* cx, int clientSock)
{	
	int i, aid = p->size;
	
	printf("Finding %i\n",aid);

	Alarm* alarm = getAlarmID(aid,cx);

	if (alarm == NULL)
		return;

	printf("Want Alarm %s\n",alarm->name);
	PacketReponseAlarm* response = malloc(sizeof(PacketReponseAlarm) + strlen(alarm->name) + 9);
	response->magic = MAGIC_NUMBER;
	response->cmd = CMD_RETRIEVE1+1;
	response->flags = 0;
	response->size = strlen(alarm->name)+9;
	response->data.epoch = alarm->epoch;
	response->data.id = alarm->id;
	response->data.type = alarm->type;
	response->data.state = alarm->state;
	response->data.nameSize = strlen(alarm->name);
	memcpy(response->data.name,alarm->name,response->data.nameSize);
	
	write(clientSock,response,(sizeof(PacketReponseAlarm) + strlen(cx->alarms[i].name)+9));
}

void FCMD_ADDACTION(Packet* p, Context* cx, int clientSock)
{
	printf("AddAction CMD, size: %i\n",p->size);
	
	int alarmID;
	read(clientSock,&alarmID,4);
	
	if (p->flags == ACTION_SOUND)
	{
		int pathSize;
		read(clientSock,&pathSize,4);
		
		char* spath = malloc(pathSize+1);
		read(clientSock,spath,pathSize);
		spath[pathSize] = 0;
		
		spath = char_replace('/',' ',spath);
		
		char* alarmIDStr = malloc(10);
		snprintf(alarmIDStr,10,"%i",alarmID);
		
		buildSection(cx->ini,"ACTIONSOUND",2,"ALARMID",alarmIDStr,"PATH",spath);
		buildINI(cx->ini,cx->iniPath);
		rebuildState(cx);
		
		printf("Added Sound[%i]: %s\n",pathSize,spath);
		
		free(alarmIDStr);
		free(spath);
	}
}

//retrieves a list of all alarms currently configured
void FCMD_RETRIEVE(Packet* p, Context* cx, int clientSock)
{	
	int x, tSize = 0, sSize = 0;
	Alarm* alarm = cx->alarms;
	
	for (x = 0; x < cx->alarmCount; x++)
	{
		tSize += strlen(alarm->name)+10;
		printf("tSize: %i\n",tSize);
		alarm = alarm->next;
	}
	
	PacketAlarmUpdate* newPack = malloc(tSize + sizeof(PacketAlarmUpdate));
	newPack->magic=0x4D524C41;
	newPack->cmd=CMD_RETRIEVE+1;
	newPack->dataSize=tSize;
	newPack->dataCount=cx->alarmCount;
	
	char* dataPtr = (char*)newPack->data;
	alarm = cx->alarms;
	
	for (x = 0; x < cx->alarmCount; x++)
	{
		AlarmData* data = (AlarmData*)dataPtr;
		data->epoch = alarm->epoch;
		data->id = alarm->id;
		data->type = alarm->type;
		data->nameSize = strlen(alarm->name);
		data->state = alarm->state;
		memcpy(data->name,alarm->name,data->nameSize);
		
		dataPtr += data->nameSize+9;
		alarm = alarm->next;
	}
	
	write(clientSock,newPack,sizeof(PacketAlarmUpdate) + tSize);
}
