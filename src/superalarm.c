#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <string.h>
#include <poll.h>

#include "superalarm.h"
#include "actions/sdl_addon.h"
#include "actions/wave.h"
#include "actions/actions.h"

// 
Alarm* getAlarmID(int aid, Context* cx)
{
	int i;
	Alarm* ptr = cx->alarms;
	
	for (i = 0; i < cx->alarmCount; i++)
	{
		if (ptr->id == aid)
			return ptr;
		ptr = ptr->next;
	}
	return NULL;
}

char* char_replace(char pattern, char replacement, char* string)
{
	int length = strlen(string);
	int i;

	for (i = 0; i < length; i++)
	{
		if (string[i] == pattern)
			string[i] = replacement;
	}
	
	return string;
}

char* fixspace_string(char* string)
{
	int length = strlen(string);
	char* out = malloc(length+1);
	int p, i = 0;
	for (p = 0; p < length; i++)
	{
		if (string[p] == '%')
		{
			if (string[p+1] == '2' && string[p+2] == '0')
			{
				out[i] = ' ';
				p+=3;
			}
		}
		else
			out[i] = string[p++];
	}
	out[i] = 0;
	free(string);
	return out;
}

//Disable audio and only listen for incoming connections
int killAudio(struct pollfd** pfd, Context* cx)
{
	if (*pfd != 0)
		free(*pfd);
	*pfd = malloc(sizeof(struct pollfd));
	(*pfd)->fd = cx->listener;
	(*pfd)->events = POLLIN;
	return 1;
}

//Enable audio as well as listen for incoming connections (if using SDL, this just calls killAudio)
int setupAudio(struct pollfd** pfd, Context* cx)
{
	#ifndef USE_SDL
	if (*pfd != 0)
		free(*pfd);
	int size = wavSetupDesc(pfd,1);
	//int wavDesc = pfd->fd;
	(*pfd)[size-1].fd = cx->listener;
	(*pfd)[size-1].events = POLLIN;
	return size;
	#else
	return killAudio(pfd,cx);
	#endif
}

// uses poll() to block until either a packet arrives or an alarm has expired, or if applicable, audio buffer is empty
void onWait(Context* cx)
{
	cx->listener = socket(AF_INET,SOCK_STREAM, 0);
	struct in_addr ipAddr;
		ipAddr.s_addr = INADDR_ANY;
	struct sockaddr_in serverAddress;
		serverAddress.sin_family = AF_INET;
		serverAddress.sin_port = htons(cx->port);
		serverAddress.sin_addr = ipAddr;
	bind(cx->listener,(struct sockaddr*)&serverAddress,(socklen_t)sizeof(struct sockaddr_in));
	listen(cx->listener,1);
	
	struct sockaddr_in clientInfo;
		socklen_t clientInfoSize = sizeof(struct sockaddr_in);
		
	int n,i;
	int timeout = -1;													//timeout, used to wakeup from poll after this many milliseconds (-1 means never wake)
	if (cx->nextTrigger != 0)
		timeout = (cx->nextTrigger->epoch - time(0)) * 1000;			//If an alarm exists update the timeout so that we wakeup when it triggers
	
	struct pollfd* pfd = 0;
	int size = 1;
	
	size = killAudio(&pfd,cx);											//Set up initial events
	
	// Main loop
	while (1)
	{
		if (cx->nextTrigger != 0 && cx->nextTrigger->epoch-time(0) > 0)
			n = poll(pfd,size,timeout);									//tell the kernel too pause the program until an event occurs, or the alarm timeout elapses
		else
			n = poll(pfd,size,-1);										//same thing except no timeout because no alarms exist
		
		for (i = 0; i < size; i++)
		{
			if (pfd[i].fd == cx->listener && pfd[i].revents != 0)		//check if a connection has been made, and conenct if that is the case
				onConnect(cx);
			#ifndef USE_SDL
			else if (pfd[i].revents != 0)								//check if the sound buffer needs updating
			{
				if (wavFill())											//and fill the buffer if it is
					size = killAudio(&pfd,cx);							//wavFill() will return 1 when the audio clip has finished, if this is the case we diable audio
			}
			#endif
		}
		
		if (cx->nextTrigger != 0)										//check if any alarm exists
		{
			if (cx->nextTrigger->epoch-time(0) > 0)						//if so, check if the alarm has elapsed
				timeout = (cx->nextTrigger->epoch-time(0)) * 1000;		//if it hasn't, update sleep timer,
			else
			{
				size = setupAudio(&pfd,cx);								//otherwise trigger the alarm, alsa re-enable audio
				onAlarm(cx);											
			}
		}
	}
}

// Used to check if an alarm has expired and then calls its action handlers
void onAlarm(Context* cx)
{	
	printf("Alarm Expired, %s\n",cx->nextTrigger->name);
	
	int i;
	printf("Total Actions: %i\n",cx->actionCount);
	for (i = 0; i < cx->actionCount; i++)
	{
		if (cx->actionData[i].alarmID == cx->nextTrigger->id)
		{
			printf("Found One!\n");
			cx->actionData[i].onExpire(cx->nextTrigger,cx->actionData[i].data);
			removeSection(cx->ini,cx->actionData[i].sectionIndex);
		}
	}
	
	//playWAV("test.wav");
	
	removeSection(cx->ini,cx->nextTrigger->sectionIndex);
	buildINI(cx->ini,cx->iniPath);
	rebuildState(cx);
}

// Called when a packet arrives, reads the header, and then executes the matching commands as found in functions.c
void onConnect(Context* cx)
{	                                                                                                                                                                                                                                                                           
	struct sockaddr_in clientInfo;
	socklen_t clientInfoSize = sizeof(struct sockaddr_in);
	
	int clientSocket = accept(cx->listener,(struct sockaddr*)&clientInfo,&clientInfoSize);
	
	printf("Connection.\n");
	
	Packet* newPack = malloc(sizeof(Packet));
	
	read(clientSocket,newPack,sizeof(Packet));
	
	if (newPack->magic == MAGIC_NUMBER)
	{
		printf("ALRM Message Received.\n");
		
		switch (newPack->cmd)
		{
			case CMD_RETRIEVE:
				FCMD_RETRIEVE(newPack,cx,clientSocket);
				break;
			case CMD_RETRIEVE1:
				FCMD_RETRIEVE1(newPack,cx,clientSocket);
				break;
			case CMD_DELETEID:
				FCMD_DELETEID(newPack,cx,clientSocket);
				break;
			case CMD_LISTSOUND:
				FCMD_LISTSOUND(newPack,cx,clientSocket);
				break;
			case CMD_ADDALARM:
				FCMD_ADDALARM(newPack,cx,clientSocket);
				break;
			case CMD_ADDACTION:
				FCMD_ADDACTION(newPack,cx,clientSocket);
				break;
			default:
				printf("Unknown/bad Command, %i.\n",newPack->cmd);
				break;
		}
	}
	
	free(newPack);
	
	close(clientSocket);
}

// Parses the INI file to build the database of alarms and other settings
int rebuildState(Context* cx)
{
	int i,x;
	uint32_t lastEpoch = -1;
	ConfFile* ini = parseINI(cx->iniPath);
	Alarm* alarmPtr = cx->alarms;
	
	cx->nextTrigger = 0;
	
	if (ini == NULL)
		return -1;
	
	if (alarmPtr != NULL)
	{
		Alarm* tPtr;
		
		for (i = 0; i < cx->alarmCount; i++)
		{
			tPtr = alarmPtr->next;
			free(alarmPtr);
			alarmPtr = tPtr;
		}
	}
	
	cx->alarmCount = 0;
	cx->alarms = malloc(sizeof(Alarm));
	alarmPtr = cx->alarms;
	
	cx->actionCount = 0;
	
	for (i = 0; i < ini->sectionCount; i++)
	{
		if (strcmp(ini->sections[i].name,"ALARM") == 0)
		{			
			for (x = 0; x < ini->sections[i].propertyCount; x++)
			{
				if (strcmp(ini->sections[i].properties[x].name,"NAME") == 0)
				{
					alarmPtr->name = malloc(strlen(ini->sections[i].properties[x].value)+1);
					memcpy(alarmPtr->name,ini->sections[i].properties[x].value,strlen(ini->sections[i].properties[x].value)+1);
					printf("%s:%s\n",alarmPtr->name,ini->sections[i].properties[x].value);
				}
				else if (strcmp(ini->sections[i].properties[x].name,"EPOCH") == 0)
				{
					alarmPtr->epoch = atoi(ini->sections[i].properties[x].value);
					
					if (alarmPtr->epoch < time(0) && alarmPtr->state != STATE_DISABLED)
						alarmPtr->state = STATE_MISSED;
				}
				else if (strcmp(ini->sections[i].properties[x].name,"TYPE") == 0)
					alarmPtr->type = atoi(ini->sections[i].properties[x].value);
				else if (strcmp(ini->sections[i].properties[x].name,"ID") == 0)
				{
					alarmPtr->id = atoi(ini->sections[i].properties[x].value);
					if (alarmPtr->id > cx->nextFreeID-1)
						cx->nextFreeID = alarmPtr->id+1;
				}
				else if (strcmp(ini->sections[i].properties[x].name,"STATE") == 0)
				{
					if (alarmPtr->state != STATE_MISSED) 
						alarmPtr->state = atoi(ini->sections[i].properties[x].value);
				}
				else
					printf("Rubbish property %s\n",ini->sections[i].properties[x].name);
			}
			
			alarmPtr->sectionIndex = i;
			alarmPtr->next = malloc(sizeof(Alarm));
			alarmPtr = alarmPtr->next;
			cx->alarmCount++;
		}
		else if (strcmp(ini->sections[i].name,"CONF") == 0)
		{
			for (x = 0; x < ini->sections[i].propertyCount; x++)
			{
				if (strcmp(ini->sections[i].properties[x].name,"PORT") == 0)
					cx->port = atoi(ini->sections[i].properties[x].value);
				else if (strcmp(ini->sections[i].properties[x].name,"SNDPATH") == 0)
				{
					cx->soundPath = malloc(strlen(ini->sections[i].properties[x].value)+1);
					strcpy(cx->soundPath,ini->sections[i].properties[x].value);
				}
				else
					printf("Rubbish property %s\n",ini->sections[i].properties[x].name);
			}
		}
		else if (strcmp(ini->sections[i].name,"ACTIONSOUND") == 0)
		{
			cx->actionData = realloc(cx->actionData,(++cx->actionCount)*sizeof(Action));
			cx->actionData[cx->actionCount-1].name = "Play Sounds";
			cx->actionData[cx->actionCount-1].onExpire = actionSoundExpire;
			cx->actionData[cx->actionCount-1].onCancel = actionSoundDelete;
			cx->actionData[cx->actionCount-1].sectionIndex = i;
			
			for (x = 0; x < ini->sections[i].propertyCount; x++)
			{
				if (strcmp(ini->sections[i].properties[x].name,"PATH") == 0)
				{
					cx->actionData[cx->actionCount-1].data = malloc(strlen(ini->sections[i].properties[x].value)+1);
					strcpy(cx->actionData[cx->actionCount-1].data,ini->sections[i].properties[x].value);
				}
				else if (strcmp(ini->sections[i].properties[x].name,"ALARMID") == 0)
				{
					cx->actionData[cx->actionCount-1].alarmID = atoi(ini->sections[i].properties[x].value);
				}
				else
					printf("Rubbish property %s\n",ini->sections[i].properties[x].name);
			}
			
			printf("New Action for %i, argument: %s\n",cx->actionData[cx->actionCount-1].alarmID,cx->actionData[cx->actionCount-1].data);
		}
		else
			printf("Rubbish Section %s\n",ini->sections[i].name);
	}
	
	alarmPtr = cx->alarms;
	
	for (i = 0; i < cx->alarmCount; i++)
	{
		if (cx->alarms[i].epoch < lastEpoch && cx->alarms[i].state == 0)
		{
			cx->nextTrigger = &cx->alarms[i];
			lastEpoch = cx->alarms[i].epoch;
			printf("New Early Time! %i=%i\n",cx->alarms[i].epoch-time(0),cx->nextTrigger->epoch-time(0));
		}
	}
	
	printf("Config loaded, %i alarms found:",cx->alarmCount);
	for (i = 0; i < cx->alarmCount; i++)
	{
		printf(" %s,",alarmPtr->name);
		alarmPtr = alarmPtr->next;
	}
	printf("\n");
	
	free(cx->ini);
	cx->ini = ini;
	
	if (cx->port == 0)
		cx->port = 5991;
	
	return 0;
}

// Main function, creates the global context and calls onWait()
int main()
{		
	Context* cx = malloc(sizeof(Context));
	cx->alarms = malloc(sizeof(Alarm));
	cx->alarmCount;
	cx->soundPath="./";
	cx->iniPath = "config.ini";
	cx->ini = NULL;
	cx->listener = 0;
	cx->port = 0;
	cx->nextFreeID = 0;
	
	if (rebuildState(cx) == -1)
	{
		printf("Failed to build state, bad INI filename\n");
		return -1;
	}	
	
	wavInit();
	
	onWait(cx);
	
	printf("Super Alarm Service has ended.\n");
	
	wavDeinit();
	
	return 0;
}
