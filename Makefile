CFLAGS=
N_CFLAGS = 
LIBS=-lpthread -lrt -lm -ldl
SRC=src/superalarm.c src/config/confparser.c src/functions.c src/actions/actions.c


all: alsa

alsa: $(SRC)
	$(CC) $(CFLAGS) $(N_CFLAGS) $(LIBS) -lasound -g $(SRC) src/actions/wave.c -o bin/superalarm

sdl: $(SRC)
	$(CC) $(CFLAGS) $(N_CFLAGS) -DUSE_SDL $(LIBS) -lSDL2 -g $(SRC) src/actions/sdl_addon.c -o bin/superalarm

clean:
	rm -f bin/superalarm.o bin/superalarm
